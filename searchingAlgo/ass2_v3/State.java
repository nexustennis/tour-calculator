import java.util.*;

/**
 * Freight System - Generic State Class
 */

public class State<E> {

    private Node<E> node;
    private Edge<E> edge;
    private State<E> parent;

    private List<Edge<E>> jobList;
    private int jobsToDo;

    /** 
     * Contructer for the state
     * each state is given its own copy of the job list
     *
     * where the state contains all the info about the A* search at that node 
     * 
     * @param node - the node of that particular state 
     *        edge - the edge given to the state, used to determine if the state is a job state 
     *        parent - parent state
     * @pre node != null
     * @post new state is created, and jobList and jobToDo is either updated, or set to null and -1 respectively
     */
    public State(Node<E> node, Edge<E> edge, State<E> parent) {
        this.node = node;
        this.edge = edge;
        this.parent = parent;

        this.jobList = null;
        this.jobsToDo = -1;

        if(parent != null && parent.getJobList() != null) {

            List<Edge<E>> newJobList = new LinkedList<Edge<E>>();
            int i = 0;
            for(Edge<E> e : parent.getJobList()) {
                
                /* should filter out the edge of this state, 
                   therefore marking that edge as completed  
                */
                if( e.equals(this.edge) ) {
                    this.edge.setEdgeType("Job");                       
                } else {
                    newJobList.add(e);
                    i++;
                }
            }
            this.jobList = newJobList;
            this.jobsToDo = i;
        }
    }


    /* === Getters === */


    /** 
     * gets the node of this instance of the state 
     * 
     * @return node
     */
    public Node<E> getNode() {
        return this.node;
    }

    /** 
     * gets the parent state of this instance of state 
     * 
     * @return parent state
     */
    public State<E> getParent() {
        return this.parent;
    }

    /** 
     * gets the edge of this instance of state 
     * 
     * @return edge
     */
    public Edge<E> getEdge() {
        return this.edge;
    }

    /** 
     * gets the name of the state node
     * 
     * @return name of the state node 
     */
    public E getName() {
        return this.getNode().getName();
    }

    /** 
     * gets the adjancey list of the state node
     * 
     * @return list of edges connected to state node
     */
    public List<Edge<E>> getConnections() {
        return this.getNode().getConnections();
    }

    /** 
     * gets the distance from the start to the current state
     * 
     * @return dist from start
     */
    public int getGCost() {
        return this.getNode().getGCost();
    }

    /** 
     * gets the estimated distance to the goal state
     * 
     * @return estimate to goal state
     */
    public int getHCost() {
        return this.getNode().getHCost();
    }

    /** 
     * f cost = dist from start + estimate to goal state 
     * 
     * @return f cost
     */
    public int getFCost() {
        return this.getNode().getFCost();
    }

    /** 
     * the joblist of this instance of the state 
     * 
     * @return list of jobs yet to be completed
     */
    public List<Edge<E>> getJobList() {
        return this.jobList;
    }

    /** 
     * gets the number of jobs left to do
     * 
     * @return num of jobs to do
     */
    public int getJobsToDo() {
        return this.jobsToDo;
    }


    /** 
     * gets the edge cost of the state 
     * 
     *  @return edge cost
     */
    public int getEdgeCost() {
        return this.getEdge().getEdgeCost();
    }

    /* === Setters === */

    /** 
     * sets the cost from the start
     * 
     * @param x - new g cost (new cost from the start)
     * @pre x >= 0
     * @post gCost of node will be updated
     */
    public void setGCost(int x) {
        this.getNode().setGCost(x);
    }
    
    /** 
     * sets the estimated dist to the goal state
     * 
     * @param x - new h cost (new estimate to goal state)
     * @pre x >= 0, and x <= actual dist to the goal state
     * @post gCost of node will be updated
     */
    public void setHCost(int x) {
        this.getNode().setHCost(x);
    }

    /** 
     * set the jobList of this instance of the state, directedly.
     * use when the state edge and parent are null, ie the initial state 
     * 
     * @param jobList - list of jobs to be completed
     * @post jobList and jobsToDo will be updated if jobList != null
     */
    public void setJobList(List<Edge<E>> jobList) {
        if(jobList != null) {
            List<Edge<E>> newJobList = new LinkedList<Edge<E>>();
            int i = 0;
            for(Edge<E> e : jobList) {
                newJobList.add(e);
                i++;
            }
            this.jobList = newJobList;
            this.jobsToDo = i;
        }
    }

    /* === Methods === */

    /** 
     * equals method - equal if names are equals
     * 
     * @param obj - to be compared 
     * @return true or false depending on whether the states names are equal
     */
    @Override
    public boolean equals(Object obj){
        // self check
        if(this == obj) return true;
        // null check
        if(obj == null) return false;
        // type check and cast
        if(this.getClass() != obj.getClass()) return false;
        State<?> o = (State<?>) obj; 
        
        // field comparison
        if ( Objects.equals(this.getName(),o.getName()) ) return true;
        return false;
    }

    /** 
     * gets the children of the current state
     * 
     * @return List of of the states that following the current state
     */
    public List<State<E>> getChildren() {

        List<State<E>> children = new LinkedList<State<E>>();

        for(Edge<E> e : this.getConnections()){

            State<E> child = new State<E>(e.getEnd(),e,this);
            
            children.add(child);
        }

        return children;
    }
    
}