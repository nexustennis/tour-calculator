import java.util.*;

/**
 * Concrete Generic Heuristic
 */

public class HeuristicB<E> implements Heuristic<E> {

    /** 
     * Estimates the distance to the goal state, based on the current state 
     * 
     * sums the costs of the jobs that have to be completed from the current state
     *
     * This heuristic is admissable because it calculate the cost to the goal state 
     * and sum of job costs <= actual cost of travelling to complete each job
     *
     * this is the current heuristic in use
     * 
     * @param state - current state, includes the list of to be completed jobs
     * @pre state != null
     * @return heuristic cost
     */
    @Override
    public int getBestEstimate(State<E> state) {

        double result = 0;

        for(Edge<E> e : state.getJobList()) {
            result += e.getEdgeCost();
        }

        return (int) (result);
    }
}