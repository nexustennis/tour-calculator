import java.util.*;

/** 
 * Freight System - generic A* search interface
 */
public interface AStar<E> {
    
    /** 
     * This method calculates the best path through a graph 
     * given a subset of edges that must be passed through
     * 
     * @param g - graph to be searched 
     *        start - starting node label
     *        jobList - subset of the graph edges, that must be covered in return path
     *        h - heuristic used in A* search
     * @pre g != null, start != null, jobList != null, h != null
     * @return List of generic edges
     */
    List<Edge<E>> bestPath(Graph<E> g, E start, List<Edge<E>> jobList, Heuristic<E> h);

}