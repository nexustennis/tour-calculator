import java.util.*;

/**
 * Freight System - Output Class, used for delegation
 */

public class Output {

    /** 
     * prints the path
     * 
     * @param p - list of edges travelled in A* search
     * @pre p contains valid edges
     * @post prints out either the path travelled or no solution if p = null
     */
    public void printPath(List<Edge<String>> p) {

        if(p != null) {

            for(Edge<String> e : p) {
                System.out.print("\n" +e.toString()) ;
            }
            System.out.println();
        } else {
            System.out.print("No Solution");
        }
    }
}
