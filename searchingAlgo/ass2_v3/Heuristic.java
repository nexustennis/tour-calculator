/**
 * Generic Interface for heuristic
 * 
 */

public interface Heuristic<E> {

    /** 
     * Estimates the distance to the goal state, based on the current state 
     * 
     * @param state - current state
     * @pre state != null
     * @return heuristic cost
     */
    int getBestEstimate(State<E> state);

}
