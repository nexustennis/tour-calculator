import java.util.*;

/**
 * Concrete generic graph represention
 */

public class FreightSystemGraph<E> implements Graph<E> {

    private List< Node<E> > nodeList;
    private List< Edge<E> > jobList; 


    /** 
     * Constructor for the graph
     * 
     * @post new instance of the graph will be created
     */
    public FreightSystemGraph() {
        this.nodeList = new LinkedList<Node<E>>();
        this.jobList = new LinkedList< Edge<E> >();
    }

    /** 
     * adds a node to the graph
     * 
     * @param node - label to identify the node
     *        nodeCost - the unloading cost of the node
     * @pre node != null, nodeCost >= 0
     * @post nodeList is update with new node
     */
    @Override
    public void addNode(E node, int nodeCost) {
        // since "node" is a arbitary object of type E
        // we have to first create a new node instance then add
        // this adds a node - where "node" is the name and with a list of neighbours
        this.nodeList.add( new Node<E>(node,nodeCost) );
    }


    /** 
     * adds a edge to the graph
     * 
     * @param node1 - label for the start node of the edge
     *        node2 - label for the end node of the edge
     *        edgeCost - the weighting of the node, in this case the travel cost
     * @pre node1 and node2 exists within the graph, edgeCost > 0
     * @post the connections are added to the adjancey list of the nodes, undirected 
     */
    @Override
    public void addEdge(E node1, E node2, int edgeCost) {
        Node<E> start = findNode(node1);
        Node<E> end = findNode(node2);

        start.addConnection(start,end,edgeCost);
        end.addConnection(end,start,edgeCost);
    }


    /** 
     * Searches the graph for the node, based on the given label
     * 
     * @param name - label to identify node
     * @return either the node, if found, or null if not found
     */
    @Override
    public Node<E> findNode(E name) {

        for(Node<E> node : this.nodeList) {
            if(Objects.equals(node.getName(),name)) return node;
        }
        return null;
    }

    /** 
     * Searches the graph for a specified edge
     * 
     * @param n1 - label to identify start node
     *        n2 - label to identify end node
     * @return either the edge, if found, or null if not found
     */
    private Edge<E> findEdge(E n1, E n2) {
        Node<E> start = findNode(n1);
        Node<E> end = findNode(n2);

        for(Edge<E> e : start.getConnections() ) {
            if( Objects.equals(e.getEnd().getName(),end.getName()) ) {
                return e;
            }
        }
        return null;
    }

    /** 
     * Adds a graph edge to the list of jobs 
     * 
     * @param x - label to identify start node
     *        y - label to identify end node
     * @pre x and y are existing nodes and are connected by an edge
     * @post adds a job to the job list
     */
    public void addJob(E x, E y) {      
        jobList.add(findEdge(x,y));
    }

    /** 
     * get the job list of edges that must be cover in the A* search 
     * 
     * @return jobList
     */
    public List<Edge<E>> getJobList() {
        return this.jobList;
    }
}