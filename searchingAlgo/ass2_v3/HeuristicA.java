import java.util.*;


/**
 * Heuristic >>>>>> Note: unfinished and not used 
 * 
 */



/*  
    get children of the current state
    recursively determine the dist to the next node with a job
*/

public class HeuristicA<E> implements Heuristic<E> {

    /** 
     *
     * 
     * @param 
     * @pre 
     * @post
     * @return
     */
    @Override
    public int getBestEstimate(State<E> state) {


        List<State<E>> children = state.getChildren();

        sortByJobs(children);

        int result = predictCost( (LinkedList<State<E>>) children);     

        //System.out.println("hCost: " + result);

        return result;
    }

    /** 
     *
     * 
     * @param 
     * @pre 
     * @post
     * @return
     */
    private int predictCost(LinkedList<State<E>> children) {

        State<E> option1 = children.poll();
        State<E> option2 = children.poll();

        if(option1.getJobsToDo() < option2.getJobsToDo()) {
            return option1.getEdgeCost();
        } else {

            List<State<E>> next = option1.getChildren();

            this.sortByJobs(next);

            int result = option1.getEdgeCost() + this.predictCost( (LinkedList<State<E>>) next);

            return result;
        }
    }

    /** 
     *
     * 
     * @param 
     * @pre 
     * @post
     * @return
     */
    private void sortByJobs(List<State<E>> children) {

        Collections.sort(children, new Comparator<State<E>>() {
            @Override 
            public int compare(State<E> s1, State<E> s2) {
                return s1.getJobsToDo() - s2.getJobsToDo();
            }
        });
    }
}