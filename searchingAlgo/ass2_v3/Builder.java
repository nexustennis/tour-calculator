import java.util.*;

/** 
 * Freight System - Builder class, builds the graph, used for delegation
 */

public class Builder {

    private FreightSystemGraph<String> map;


    /** 
     * Constructor for builder class
     * 
     * @post concrete graph of nodes labelled with string is initialised
     */

    public Builder () {
        this.map = new FreightSystemGraph<String>();
    }

    /** 
     * Builds the graph
     * 
     * @param input - array of strings, contains the scanned input
     * @pre input != null, input is valid according to the spec 
     * @post Either a node is added, a edge is added, a job is added
     */
    public void buildMap(String[] input) {

        String todo = input[0];

        if(Objects.equals(todo,"Unloading")) this.map.addNode(input[2], Integer.parseInt(input[1]));

        else if(Objects.equals(todo,"Cost")) this.map.addEdge(input[2], input[3], Integer.parseInt(input[1]));

        else if(Objects.equals(todo,"Job")) this.map.addJob(input[1],input[2]);
    }
    
    /** 
     * Get the graph
     * 
     * @pre graph is initialised
     * @return graph is returned 
     */
    public FreightSystemGraph<String> getMap() {
        return this.map;
    }

    /** 
     * get the list of jobs
     * 
     * @pre graph (graph contains jobList) is initialised
     * @return joblist is returned 
     */
    public List<Edge<String>> getJobList() {
        return this.getMap().getJobList();
    }


}