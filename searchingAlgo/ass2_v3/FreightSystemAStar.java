import java.util.*;


public class FreightSystemAStar<E> implements AStar<E> {

    private static final int MAX_NODES = 100000;


    /** 
     * This method calculates the best path through a graph 
     * given a subset of edges that must be passed through
     *
     * Implementation of the A* Algorithm, the goal state is the where the 
     * path travelled covers the subset of edges, joblist.
     * 
     * @param g - graph to be searched 
     *        start - starting node label
     *        jobList - subset of the graph edges, that must be covered in return path
     *        h - heuristic used in A* search
     * @pre g != null, start != null, jobList != null, h != null
     * @return List of generic edges
     */
    @Override 
    public List<Edge<E>> bestPath(Graph<E> g, E start, List<Edge<E>> jobList, Heuristic<E> h) {

        /* Setting up open priority queue and closed list */
        PriorityQueue< State<E> > open = new PriorityQueue< State<E> >(100, new StateComparator<E>() );
        List< State<E> > closed = new LinkedList< State<E> >();

        /* keeps track of nodes popped of the queue */
        int nExpanded = 0;

        /* Add initial state to the pq and setting the g and h costs */
        State<E> curr = new State<E>(g.findNode(start),null,null);
        curr.setJobList(jobList);
        curr.setGCost(0);
        curr.setHCost(0);

        while(curr.getJobsToDo() != 0 ) {           
            
            

            List<Edge<E>> conns = curr.getConnections();

            for(Edge<E> e : conns) {
                /* take in edge and parent node from which all  the data i need can be accessed */
                State<E> neighbor = new State<E>(e.getEnd(),e,curr);

                Boolean inOpen = open.contains(neighbor);
                Boolean inClosed = closed.contains(neighbor);
                

                /* Dist from start */
                int gCost = curr.getGCost() + e.getEdgeCost();
                int hCost = h.getBestEstimate(neighbor);
                int fCost = gCost + hCost;

                /* if new state f cost is cheaper than the old f cost
                   or if the f cost is has not been set (not yet explored) */
                if(inClosed && fCost < curr.getFCost() ) {
                    neighbor.setGCost(gCost);
                    neighbor.setHCost(hCost);
                }
                else if (inOpen && fCost < curr.getFCost()) {
                    neighbor.setGCost(gCost);
                    neighbor.setHCost(hCost);
                } 
                else {
                    neighbor.setGCost(gCost);
                    neighbor.setHCost(hCost);
                    open.add(neighbor);
                }
            }

            /* After so many nodes give up */
            if(nExpanded > MAX_NODES) {
                return null;
            }
            
            closed.add(curr);

            /* pop highest priority state off queue (based on lowest f = g + h )*/
            curr = open.poll();
            nExpanded++;
        }

        return constructPath(curr,nExpanded,calcUnloadCost(jobList));
    }


    /** 
     * Constructs the path travelled through
     * 
     * @param state - the final state
     *        n - nodes expanded
     *        unloadCost - the unloading cost of the jobs that must be accounted for
     * @pre state != null
     * @post expanded nodes and total cost are printed
     *       note: to be fixed (my orginal intention was to do all the printing in output but I ran out of time!)
     * @return List of the travel edges
     */
    public List<Edge<E>> constructPath(State<E> state, int n, int unloadCost) {

        System.out.println(n + " expanded");

        System.out.print("cost = " + Integer.toString(state.getGCost() + unloadCost));

        LinkedList<Edge<E>> path = new LinkedList<Edge<E>>();

        State<E> curr = state;

        while(curr.getParent() != null) {

            path.addFirst(curr.getEdge());
            curr = curr.getParent();
        }
        return (List<Edge<E>>) path;

    }


    /** 
     * Calculates the unloading cost of the all the jobs completed
     * is is done at the end of the search because the unloading cost does not
     * affect the search itself (at least from my intepretation of the spec)
     * 
     * @param jobList - list of job edges 
     * @pre jobList != null, bestPath search has been completed and returned a soltuion
     * @return total unloading cost 
     */
    private int calcUnloadCost(List<Edge<E>> jobList) {
        int cost = 0;
        for(Edge<E> e : jobList) {
            cost += e.getEnd().getNodeCost();
        }
        return cost;
    }

}