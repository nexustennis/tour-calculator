import java.util.*;

/**
 * Node class - node is a generic type E
 * -> weight is unloading cost
 */

public class Node<E> {

    private E name;
    private List<Edge<E>> connections;
    private int nodeCost;

    private int fCost;
    private int gCost;
    private int hCost;

    /** 
     * 
     * 
     * @param 
     * @pre 
     * @post
     * @return
     */
    public Node(E name, int nodeCost) {
        this.name = name;
        this.nodeCost = nodeCost;
        this.connections = new LinkedList<Edge<E>>();

        /* Cost are initialised to 0
           then are set when explored */
        
        this.gCost = 0;
        this.hCost = 0;
        this.fCost = 0;
    }

    /* === Getters === */

    /** 
     * gets the name of the node
     * 
     * @return name of the node 
     */
    public E getName() {
        return this.name;
    }

    /** 
     * gets the node cost - unloading cost
     * 
     * @return node cost
     */
    public int getNodeCost() {
        return this.nodeCost;
    }

    /** 
     * gets the adjancey list of the node
     * 
     * @return list of edges connected to node
     */
    public List<Edge<E>> getConnections() {
        return this.connections;
    }


    /** 
     * gets the distance from the start to the current node
     * 
     * @return dist from start
     */
    public int getGCost() {
        return this.gCost;
    }

    /** 
     * gets the estimated distance to the goal
     * 
     * @return estimate to the goal
     */
    public int getHCost() {
        return this.hCost;
    }

    /** 
     * f cost = dist from start + estimate to the goal 
     * 
     * @return f cost
     */
    public int getFCost() {
        return this.getGCost() + this.getHCost();
    }

    /* === Setters === */

    /** 
     * sets the cost from the start
     * 
     * @param x - new g cost (new cost from the start)
     * @pre x >= 0
     * @post gCost of node will be updated
     */
    public void setGCost(int x) {
        this.gCost = x;
    }

    /** 
     * sets the estimated dist to the goal
     * 
     * @param x - new h cost (new estimate to the goal)
     * @pre x >= 0, and x <= actual dist to the goal
     * @post gCost of node will be updated
     */
    public void setHCost(int x) {
        this.hCost = x;
    }

    /* === Methods === */

    /** 
     * adds a connection into the nodes in the adjancey list
     * 
     * @param start - label for the start node of the edge 
     *        end - label for the end node of the edge
     *        edgeWeight - the weighting of the edge
     * @pre node1 and node2 exists within the graph, edgeCost > 0
     * @post the connections are added to the adjancey list of the node 
     */
    public void addConnection(Node<E> start, Node<E> end, int edgeWeight) {
        this.getConnections().add( new Edge<E>(start,end,edgeWeight) );
    }
}
