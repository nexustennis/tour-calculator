import java.util.*;


/**
 * Freight System - Generic Edge class, represents edges with a starting node and ending node
 */

public class Edge<E> {  
    
    private int edgeCost;
    private Node<E> start;
    private Node<E> end;
    private String type;

    /** 
     * Edge constructor
     * 
     * @param start - starting node
     *        end - ending node
     *        edgeCost - the cost of the edge
     * @pre start != null, end != null, edgeCost > 0
     * @post a instance of the edge class is initialised
     */
    public Edge(Node<E> start, Node<E> end, int edgeCost) {
        this.edgeCost = edgeCost;
        this.start = start;
        this.end = end;
        this.type = "Empty";
    }
    
    /* === Getters === */

    /** 
     * Gets the edges cost 
     * 
     * @pre this instance of the class has been initialised 
     * @return edge cost of this instance
     */
    public int getEdgeCost() {
        return this.edgeCost;
    }

    /** 
     * Gets the node at the start of the edge 
     * 
     * @pre this instance of the class has been initialised 
     * @return start node
     */
    public Node<E> getStart() {
        return this.start;
    }

    /** 
     * Gets the node at the end of the edge 
     * 
     * @pre this instance of the class has been initialised 
     * @return end node
     */
    public Node<E> getEnd() {
        return this.end;
    }

    /** 
     * Gets the type of edges, either "Empty" or "Job" 
     * 
     * @pre this instance of the class has been initialised 
     * @return type string
     */
    public String getEdgeType() {
        return this.type;
    }

    /* === Setters === */


    /** 
     * Set the type of edge, either "Empty" or "Job" 
     * 
     * @param x - type
     * @pre this instance of the class has been initialised, x != null
     * @post type param will be updated
     * @return type string
     */
    public void setEdgeType(String x) {
        this.type = x;
    }

    /* === Methods === */

    /** 
     * Equals method
     * 
     * @param obj - object to be compared
     * @return true or false depending on whether the starting and ending nodes are equal
     */
    @Override
    public boolean equals(Object obj){
        // self check
        if(this == obj) return true;
        // null check
        if(obj == null) return false;
        // type check and cast
        if(getClass() != obj.getClass()) return false;
        Edge<?> o = (Edge<?>) obj; 
        
        // field comparison
        if ( this.getStart().equals(o.getStart()) && this.getEnd().equals(o.getEnd()) ) return true;
        return false;
    }

    /** 
     * To String method
     *
     * @return formatted string (according to the spec) with the edge type and edge nodes 
     */
    public String toString() {
        if (this == null) return "null";
        else return this.getEdgeType() + " " + this.getStart().getName() + " to " + this.getEnd().getName() ; 
    }

}
