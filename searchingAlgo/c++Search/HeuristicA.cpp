//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "HeuristicA.h"

HeuristicA<E>::ComparatorAnonymousInnerClass::ComparatorAnonymousInnerClass(HeuristicA<E*> *outerInstance)
{
	this->outerInstance = outerInstance;
}

int HeuristicA<E>::ComparatorAnonymousInnerClass::compare(State<E*> *s1, State<E*> *s2)
{
	return s1->getJobsToDo() - s2->getJobsToDo();
}
