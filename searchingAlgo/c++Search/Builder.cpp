//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Builder.h"
#include "FreightSystemGraph.h"
#include "Edge.h"

Builder::Builder()
{
	this->map = new FreightSystemGraph<std::wstring>();
}

void Builder::buildMap(std::vector<std::wstring> &input)
{

	std::wstring todo = input[0];

	if (Objects::equals(todo,L"Unloading"))
	{
		this->map->addNode(input[2], std::stoi(input[1]));
	}

	else if (Objects::equals(todo,L"Cost"))
	{
		this->map->addEdge(input[2], input[3], std::stoi(input[1]));
	}

	else if (Objects::equals(todo,L"Job"))
	{
		this->map->addJob(input[1],input[2]);
	}
}

FreightSystemGraph<std::wstring> *Builder::getMap()
{
	return this->map;
}

std::vector<Edge<std::wstring>*> Builder::getJobList()
{
	return this->getMap()->getJobList();
}
