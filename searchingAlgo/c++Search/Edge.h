#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Node.h"
#include <string>

/// <summary>
/// Freight System - Generic Edge class, represents edges with a starting node and ending node
/// </summary>

template<typename E>
class Edge
{

private:
	int edgeCost = 0;
	Node<E> *start;
	Node<E> *end;
	std::wstring type;

	/// <summary>
	/// Edge constructor
	/// </summary>
	/// <param name="start"> - starting node
	///        end - ending node
	///        edgeCost - the cost of the edge
	/// @pre start != null, end != null, edgeCost > 0
	/// @post a instance of the edge class is initialised </param>
public:
	virtual ~Edge()
	{
		delete start;
		delete end;
	}

	Edge(Node<E> *start, Node<E> *end, int edgeCost)
	{
		this->edgeCost = edgeCost;
		this->start = start;
		this->end = end;
		this->type = L"Empty";
	}

	/* === Getters === */

	/// <summary>
	/// Gets the edges cost 
	/// 
	/// @pre this instance of the class has been initialised </summary>
	/// <returns> edge cost of this instance </returns>
	virtual int getEdgeCost()
	{
		return this->edgeCost;
	}

	/// <summary>
	/// Gets the node at the start of the edge 
	/// 
	/// @pre this instance of the class has been initialised </summary>
	/// <returns> start node </returns>
	virtual Node<E> *getStart()
	{
		return this->start;
	}

	/// <summary>
	/// Gets the node at the end of the edge 
	/// 
	/// @pre this instance of the class has been initialised </summary>
	/// <returns> end node </returns>
	virtual Node<E> *getEnd()
	{
		return this->end;
	}

	/// <summary>
	/// Gets the type of edges, either "Empty" or "Job" 
	/// 
	/// @pre this instance of the class has been initialised </summary>
	/// <returns> type string </returns>
	virtual std::wstring getEdgeType()
	{
		return this->type;
	}

	/* === Setters === */


	/// <summary>
	/// Set the type of edge, either "Empty" or "Job" 
	/// </summary>
	/// <param name="x"> - type
	/// @pre this instance of the class has been initialised, x != null
	/// @post type param will be updated </param>
	/// <returns> type string </returns>
	virtual void setEdgeType(const std::wstring &x)
	{
		this->type = x;
	}

	/* === Methods === */

	/// <summary>
	/// Equals method
	/// </summary>
	/// <param name="obj"> - object to be compared </param>
	/// <returns> true or false depending on whether the starting and ending nodes are equal </returns>
	bool equals(void *obj) override
	{
		// self check
		if (this == obj)
		{
			return true;
		}
		// null check
		if (obj == nullptr)
		{
			return false;
		}
		// type check and cast
		if (getClass() != obj->getClass())
		{
			return false;
		}
//JAVA TO C++ CONVERTER TODO TASK: Java wildcard generics are not converted to C++:
//ORIGINAL LINE: Edge<?> o = (Edge<?>) obj;
		Edge<?> *o = static_cast<Edge<?>*>(obj);

		// field comparison
		if (this->getStart()->equals(o->getStart()) && this->getEnd()->equals(o->getEnd()))
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// To String method
	/// </summary>
	/// <returns> formatted string (according to the spec) with the edge type and edge nodes  </returns>
	std::wstring toString() override
	{
		if (this == nullptr)
		{
			return L"null";
		}
		else
		{
			return this->getEdgeType() + L" " + this->getStart()->getName() + L" to " + this->getEnd()->getName();
		}
	}

};
