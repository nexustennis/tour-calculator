#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Heuristic.h"
#include "State.h"
#include <vector>
#include <list>
#include <algorithm>

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class State;

/// <summary>
/// Heuristic >>>>>> Note: unfinished and not used 
/// 
/// </summary>



/*  
    get children of the current state
    recursively determine the dist to the next node with a job
*/

template<typename E>
class HeuristicA : public Heuristic<E>
{

	/// 
	/// 
	/// @param 
	/// @pre 
	/// @post
	/// @return </param>
public:
	int getBestEstimate(State<E> *state) override
	{


		std::vector<State<E>*> children = state->getChildren();

		sortByJobs(children);

		int result = predictCost(static_cast<std::list<State<E>*>>(children));

		//System.out.println("hCost: " + result);

		return result;
	}

	/// 
	/// 
	/// @param 
	/// @pre 
	/// @post
	/// @return </param>
private:
	int predictCost(std::list<State<E>*> &children)
	{

		State<E> *option1 = children.pop_front();
		State<E> *option2 = children.pop_front();

		if (option1->getJobsToDo() < option2->getJobsToDo())
		{
			return option1->getEdgeCost();
		}
		else
		{

			std::vector<State<E>*> next = option1->getChildren();

			this->sortByJobs(next);

			int result = option1->getEdgeCost() + this->predictCost(static_cast<std::list<State<E>*>>(next));

			return result;
		}
	}

	/// 
	/// 
	/// @param 
	/// @pre 
	/// @post
	/// @return </param>
	void sortByJobs(std::vector<State<E>*> &children)
	{

//JAVA TO C++ CONVERTER TODO TASK: The 'Compare' parameter of std::sort produces a boolean value, while the Java Comparator parameter produces a tri-state result:
//ORIGINAL LINE: Collections.sort(children, new Comparator<State<E>>()
		ComparatorAnonymousInnerClass tempVar(this);
		std::sort(children.begin(), children.end(), &tempVar);
	}

private:
	class ComparatorAnonymousInnerClass : public Comparator<State<E*>*>
	{
	private:
		HeuristicA<E*> *outerInstance;

	public:
		ComparatorAnonymousInnerClass(HeuristicA<E*> *outerInstance);

		virtual int compare(State<E*> *s1, State<E*> *s2);
	};
};
