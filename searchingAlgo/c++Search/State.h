#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Node.h"
#include "Edge.h"
#include <vector>
#include <list>

/// <summary>
/// Freight System - Generic State Class
/// </summary>

template<typename E>
class State
{

private:
	Node<E> *node;
	Edge<E> *edge;
	State<E> *parent;

	std::vector<Edge<E>*> jobList;
	int jobsToDo = 0;

	/// <summary>
	/// Contructer for the state
	/// each state is given its own copy of the job list
	/// 
	/// where the state contains all the info about the A* search at that node 
	/// </summary>
	/// <param name="node"> - the node of that particular state 
	///        edge - the edge given to the state, used to determine if the state is a job state 
	///        parent - parent state
	/// @pre node != null
	/// @post new state is created, and jobList and jobToDo is either updated, or set to null and -1 respectively </param>
public:
	virtual ~State()
	{
		delete node;
		delete edge;
		delete parent;
	}

	State(Node<E> *node, Edge<E> *edge, State<E> *parent)
	{
		this->node = node;
		this->edge = edge;
		this->parent = parent;

		this->jobList.clear();
		this->jobsToDo = -1;

		if (parent != nullptr && parent->getJobList().size() > 0)
		{

			std::vector<Edge<E>*> newJobList = std::list<Edge<E>*>();
			int i = 0;
			for (auto e : parent->getJobList())
			{

				/* should filter out the edge of this state, 
				   therefore marking that edge as completed  
				*/
				if (e->equals(this->edge))
				{
					this->edge->setEdgeType(L"Job");
				}
				else
				{
					newJobList.push_back(e);
					i++;
				}
			}
			this->jobList = newJobList;
			this->jobsToDo = i;
		}
	}


	/* === Getters === */


	/// <summary>
	/// gets the node of this instance of the state 
	/// </summary>
	/// <returns> node </returns>
	virtual Node<E> *getNode()
	{
		return this->node;
	}

	/// <summary>
	/// gets the parent state of this instance of state 
	/// </summary>
	/// <returns> parent state </returns>
	virtual State<E> *getParent()
	{
		return this->parent;
	}

	/// <summary>
	/// gets the edge of this instance of state 
	/// </summary>
	/// <returns> edge </returns>
	virtual Edge<E> *getEdge()
	{
		return this->edge;
	}

	/// <summary>
	/// gets the name of the state node
	/// </summary>
	/// <returns> name of the state node  </returns>
	virtual E getName()
	{
		return this->getNode()->getName();
	}

	/// <summary>
	/// gets the adjancey list of the state node
	/// </summary>
	/// <returns> list of edges connected to state node </returns>
	virtual std::vector<Edge<E>*> getConnections()
	{
		return this->getNode()->getConnections();
	}

	/// <summary>
	/// gets the distance from the start to the current state
	/// </summary>
	/// <returns> dist from start </returns>
	virtual int getGCost()
	{
		return this->getNode()->getGCost();
	}

	/// <summary>
	/// gets the estimated distance to the goal state
	/// </summary>
	/// <returns> estimate to goal state </returns>
	virtual int getHCost()
	{
		return this->getNode()->getHCost();
	}

	/// <summary>
	/// f cost = dist from start + estimate to goal state 
	/// </summary>
	/// <returns> f cost </returns>
	virtual int getFCost()
	{
		return this->getNode()->getFCost();
	}

	/// <summary>
	/// the joblist of this instance of the state 
	/// </summary>
	/// <returns> list of jobs yet to be completed </returns>
	virtual std::vector<Edge<E>*> getJobList()
	{
		return this->jobList;
	}

	/// <summary>
	/// gets the number of jobs left to do
	/// </summary>
	/// <returns> num of jobs to do </returns>
	virtual int getJobsToDo()
	{
		return this->jobsToDo;
	}


	/// <summary>
	/// gets the edge cost of the state 
	/// </summary>
	///  <returns> edge cost </returns>
	virtual int getEdgeCost()
	{
		return this->getEdge()->getEdgeCost();
	}

	/* === Setters === */

	/// <summary>
	/// sets the cost from the start
	/// </summary>
	/// <param name="x"> - new g cost (new cost from the start)
	/// @pre x >= 0
	/// @post gCost of node will be updated </param>
	virtual void setGCost(int x)
	{
		this->getNode()->setGCost(x);
	}

	/// <summary>
	/// sets the estimated dist to the goal state
	/// </summary>
	/// <param name="x"> - new h cost (new estimate to goal state)
	/// @pre x >= 0, and x <= actual dist to the goal state
	/// @post gCost of node will be updated </param>
	virtual void setHCost(int x)
	{
		this->getNode()->setHCost(x);
	}

	/// <summary>
	/// set the jobList of this instance of the state, directedly.
	/// use when the state edge and parent are null, ie the initial state 
	/// </summary>
	/// <param name="jobList"> - list of jobs to be completed
	/// @post jobList and jobsToDo will be updated if jobList != null </param>
	virtual void setJobList(std::vector<Edge<E>*> &jobList)
	{
		if (jobList.size() > 0)
		{
			std::vector<Edge<E>*> newJobList = std::list<Edge<E>*>();
			int i = 0;
			for (auto e : jobList)
			{
				newJobList.push_back(e);
				i++;
			}
			this->jobList = newJobList;
			this->jobsToDo = i;
		}
	}

	/* === Methods === */

	/// <summary>
	/// equals method - equal if names are equals
	/// </summary>
	/// <param name="obj"> - to be compared </param>
	/// <returns> true or false depending on whether the states names are equal </returns>
	bool equals(void *obj) override
	{
		// self check
		if (this == obj)
		{
			return true;
		}
		// null check
		if (obj == nullptr)
		{
			return false;
		}
		// type check and cast
		if (this->getClass() != obj->getClass())
		{
			return false;
		}
//JAVA TO C++ CONVERTER TODO TASK: Java wildcard generics are not converted to C++:
//ORIGINAL LINE: State<?> o = (State<?>) obj;
		State<?> *o = static_cast<State<?>*>(obj);

		// field comparison
		if (Objects::equals(this->getName(),o->getName()))
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// gets the children of the current state
	/// </summary>
	/// <returns> List of of the states that following the current state </returns>
	virtual std::vector<State<E>*> getChildren()
	{

		std::vector<State<E>*> children = std::list<State<E>*>();

		for (auto e : this->getConnections())
		{

			State<E> *child = new State<E>(e->getEnd(),e,this);

			children.push_back(child);
		}

		return children;
	}

};
