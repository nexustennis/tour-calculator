#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include <vector>

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class Edge;
template<typename E>
class Graph;
template<typename E>
class Heuristic;

/// <summary>
/// Freight System - generic A* search interface
/// </summary>
template<typename E>
class AStar
{

	/// <summary>
	/// This method calculates the best path through a graph 
	/// given a subset of edges that must be passed through
	/// </summary>
	/// <param name="g"> - graph to be searched 
	///        start - starting node label
	///        jobList - subset of the graph edges, that must be covered in return path
	///        h - heuristic used in A* search
	/// @pre g != null, start != null, jobList != null, h != null </param>
	/// <returns> List of generic edges </returns>
public:
	virtual std::vector<Edge<E>*> bestPath(Graph<E> *g, E start, std::vector<Edge<E>*> &jobList, Heuristic<E> *h) = 0;

};
