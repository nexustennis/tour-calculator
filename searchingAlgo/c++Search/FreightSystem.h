#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include <string>
#include <vector>
#include "exceptionhelper.h"

/// <summary>
/// Freight system - this class contains the main
/// @author z5114817
/// </summary>

class FreightSystem
{

	/// <summary>
	/// Main function - Runs Freight System
	/// </summary>
	/// <param name="String">[] args - contains input files
	/// @pre  args[0] != null </param>
	static void main(std::vector<std::wstring> &args);
};

/*
Analysis of heuristic
    
    used heuristicB, as it simple and admissable

test 1 - input1.txt

9 expanded
cost = 2320
Job Sydney to Bathurst
Empty Bathurst to Dubbo
Job Dubbo to Grafton
Job Grafton to Wagga
Empty Wagga to Sydney
Job Sydney to Wagga

real    0m0.278s
user    0m0.284s
sys 0m0.064s


test 2 - input2.txt

38 expanded
cost = 320
Job Sydney to D
Empty D to C
Job C to E
Empty E to C
Empty C to B
Empty B to Sydney
Job Sydney to B
Job B to C

real    0m0.285s
user    0m0.300s
sys 0m0.036s

    
test 3 - input3.txt

208 expanded
cost = 10143
Job Sydney to A
Job A to B
Job B to C
Empty C to D
Empty D to C
Empty C to F
Empty F to G
Job G to D
Job D to F
Job F to B

real    0m0.295s
user    0m0.280s
sys 0m0.044s


test 4 - input4.txt


No Solution
real    0m0.769s
user    0m1.416s
sys 0m0.108s




*/
