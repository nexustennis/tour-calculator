#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Heuristic.h"
#include "State.h"

/// <summary>
/// Concrete Generic Heuristic
/// </summary>

template<typename E>
class HeuristicB : public Heuristic<E>
{

	/// <summary>
	/// Estimates the distance to the goal state, based on the current state 
	/// 
	/// sums the costs of the jobs that have to be completed from the current state
	/// 
	/// This heuristic is admissable because it calculate the cost to the goal state 
	/// and sum of job costs <= actual cost of travelling to complete each job
	/// 
	/// this is the current heuristic in use
	/// </summary>
	/// <param name="state"> - current state, includes the list of to be completed jobs
	/// @pre state != null </param>
	/// <returns> heuristic cost </returns>
public:
	int getBestEstimate(State<E> *state) override
	{

		double result = 0;

		for (auto e : state->getJobList())
		{
			result += e->getEdgeCost();
		}

		return static_cast<int>(result);
	}
};
