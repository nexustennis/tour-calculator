#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Edge.h"
#include <vector>
#include <list>

/// <summary>
/// Node class - node is a generic type E
/// -> weight is unloading cost
/// </summary>

template<typename E>
class Node
{

private:
	E name;
	std::vector<Edge<E>*> connections;
	int nodeCost = 0;

	int fCost = 0;
	int gCost = 0;
	int hCost = 0;

	/// 
	/// 
	/// @param 
	/// @pre 
	/// @post
	/// @return </param>
public:
	Node(E name, int nodeCost)
	{
		this->name = name;
		this->nodeCost = nodeCost;
		this->connections = std::list<Edge<E>*>();

		/* Cost are initialised to 0
		   then are set when explored */

		this->gCost = 0;
		this->hCost = 0;
		this->fCost = 0;
	}

	/* === Getters === */

	/// <summary>
	/// gets the name of the node
	/// </summary>
	/// <returns> name of the node  </returns>
	virtual E getName()
	{
		return this->name;
	}

	/// <summary>
	/// gets the node cost - unloading cost
	/// </summary>
	/// <returns> node cost </returns>
	virtual int getNodeCost()
	{
		return this->nodeCost;
	}

	/// <summary>
	/// gets the adjancey list of the node
	/// </summary>
	/// <returns> list of edges connected to node </returns>
	virtual std::vector<Edge<E>*> getConnections()
	{
		return this->connections;
	}


	/// <summary>
	/// gets the distance from the start to the current node
	/// </summary>
	/// <returns> dist from start </returns>
	virtual int getGCost()
	{
		return this->gCost;
	}

	/// <summary>
	/// gets the estimated distance to the goal
	/// </summary>
	/// <returns> estimate to the goal </returns>
	virtual int getHCost()
	{
		return this->hCost;
	}

	/// <summary>
	/// f cost = dist from start + estimate to the goal 
	/// </summary>
	/// <returns> f cost </returns>
	virtual int getFCost()
	{
		return this->getGCost() + this->getHCost();
	}

	/* === Setters === */

	/// <summary>
	/// sets the cost from the start
	/// </summary>
	/// <param name="x"> - new g cost (new cost from the start)
	/// @pre x >= 0
	/// @post gCost of node will be updated </param>
	virtual void setGCost(int x)
	{
		this->gCost = x;
	}

	/// <summary>
	/// sets the estimated dist to the goal
	/// </summary>
	/// <param name="x"> - new h cost (new estimate to the goal)
	/// @pre x >= 0, and x <= actual dist to the goal
	/// @post gCost of node will be updated </param>
	virtual void setHCost(int x)
	{
		this->hCost = x;
	}

	/* === Methods === */

	/// <summary>
	/// adds a connection into the nodes in the adjancey list
	/// </summary>
	/// <param name="start"> - label for the start node of the edge 
	///        end - label for the end node of the edge
	///        edgeWeight - the weighting of the edge
	/// @pre node1 and node2 exists within the graph, edgeCost > 0
	/// @post the connections are added to the adjancey list of the node  </param>
	virtual void addConnection(Node<E> *start, Node<E> *end, int edgeWeight)
	{
		Edge<E> tempVar(start,end,edgeWeight);
		this->getConnections().push_back(&tempVar);
	}
};
