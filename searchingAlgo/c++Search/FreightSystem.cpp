//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "FreightSystem.h"
#include "Builder.h"
#include "AStar.h"
#include "FreightSystemAStar.h"
#include "Edge.h"
#include "HeuristicB.h"
#include "Output.h"

void FreightSystem::main(std::vector<std::wstring> &args)
{

	Scanner *sc = nullptr;
	try
	{
		FileReader tempVar(args[0]);
		sc = new Scanner(&tempVar);

		Builder *freightMap = new Builder();

		while (sc->hasNextLine())
		{
		  freightMap->buildMap(sc->nextLine()->split(L"[ ]"));
		}

		AStar<std::wstring> *aStar = new FreightSystemAStar<std::wstring>();

		HeuristicB<std::wstring> tempVar2();
		std::vector<Edge<std::wstring>*> path = aStar->bestPath(freightMap->getMap(),L"Sydney",freightMap->getJobList(),&tempVar2);

		Output *out = new Output();

		out->printPath(path);

	}
	catch (const FileNotFoundException &e)
	{

	}
//JAVA TO C++ CONVERTER TODO TASK: There is no native C++ equivalent to the exception 'finally' clause:
	finally
	{
	  if (sc != nullptr)
	  {
		  delete sc;
	  }
	}


}
