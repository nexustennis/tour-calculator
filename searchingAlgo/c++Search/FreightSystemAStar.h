#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "AStar.h"
#include "Edge.h"
#include "Graph.h"
#include "Heuristic.h"
#include "State.h"
#include "StateComparator.h"
#include <vector>
#include <list>
#include <algorithm>
#include <iostream>

template<typename E>
class FreightSystemAStar : public AStar<E>
{

private:
	static constexpr int MAX_NODES = 100000;


	/// <summary>
	/// This method calculates the best path through a graph 
	/// given a subset of edges that must be passed through
	/// 
	/// Implementation of the A* Algorithm, the goal state is the where the 
	/// path travelled covers the subset of edges, joblist.
	/// </summary>
	/// <param name="g"> - graph to be searched 
	///        start - starting node label
	///        jobList - subset of the graph edges, that must be covered in return path
	///        h - heuristic used in A* search
	/// @pre g != null, start != null, jobList != null, h != null </param>
	/// <returns> List of generic edges </returns>
public:
	std::vector<Edge<E>*> bestPath(Graph<E> *g, E start, std::vector<Edge<E>*> &jobList, Heuristic<E> *h) override
	{

		/* Setting up open priority queue and closed list */
		StateComparator<E> tempVar();
		PriorityQueue<State<E>*> *open = new PriorityQueue<State<E>*>(100, &tempVar);
		std::vector<State<E>*> closed = std::list<State<E>*>();

		/* keeps track of nodes popped of the queue */
		int nExpanded = 0;

		/* Add initial state to the pq and setting the g and h costs */
		State<E> *curr = new State<E>(g->findNode(start),nullptr,nullptr);
		curr->setJobList(jobList);
		curr->setGCost(0);
		curr->setHCost(0);

		while (curr->getJobsToDo() != 0)
		{



			std::vector<Edge<E>*> conns = curr->getConnections();

			for (auto e : conns)
			{
				/* take in edge and parent node from which all  the data i need can be accessed */
				State<E> *neighbor = new State<E>(e->getEnd(),e,curr);

				Boolean inOpen = open->contains(neighbor);
				Boolean inClosed = std::find(closed.begin(), closed.end(), neighbor) != closed.end();


				/* Dist from start */
				int gCost = curr->getGCost() + e->getEdgeCost();
				int hCost = h->getBestEstimate(neighbor);
				int fCost = gCost + hCost;

				/* if new state f cost is cheaper than the old f cost
				   or if the f cost is has not been set (not yet explored) */
				if (inClosed && fCost < curr->getFCost())
				{
					neighbor->setGCost(gCost);
					neighbor->setHCost(hCost);
				}
				else if (inOpen && fCost < curr->getFCost())
				{
					neighbor->setGCost(gCost);
					neighbor->setHCost(hCost);
				}
				else
				{
					neighbor->setGCost(gCost);
					neighbor->setHCost(hCost);
					open->add(neighbor);
				}
			}

			/* After so many nodes give up */
			if (nExpanded > MAX_NODES)
			{
				return nullptr;
			}

			closed.push_back(curr);

			/* pop highest priority state off queue (based on lowest f = g + h )*/
			curr = open->poll();
			nExpanded++;
		}

		return constructPath(curr,nExpanded,calcUnloadCost(jobList));
	}


	/// <summary>
	/// Constructs the path travelled through
	/// </summary>
	/// <param name="state"> - the final state
	///        n - nodes expanded
	///        unloadCost - the unloading cost of the jobs that must be accounted for
	/// @pre state != null
	/// @post expanded nodes and total cost are printed
	///       note: to be fixed (my orginal intention was to do all the printing in output but I ran out of time!) </param>
	/// <returns> List of the travel edges </returns>
	virtual std::vector<Edge<E>*> constructPath(State<E> *state, int n, int unloadCost)
	{

		std::wcout << n << L" expanded" << std::endl;

//JAVA TO C++ CONVERTER TODO TASK: There is no native C++ equivalent to 'toString':
		std::wcout << L"cost = " << Integer::toString(state->getGCost() << unloadCost);

		std::list<Edge<E>*> path;

		State<E> *curr = state;

		while (curr->getParent() != nullptr)
		{

			path.push_front(curr->getEdge());
			curr = curr->getParent();
		}
		return static_cast<std::vector<Edge<E>*>>(path);

	}


	/// <summary>
	/// Calculates the unloading cost of the all the jobs completed
	/// is is done at the end of the search because the unloading cost does not
	/// affect the search itself (at least from my intepretation of the spec)
	/// </summary>
	/// <param name="jobList"> - list of job edges 
	/// @pre jobList != null, bestPath search has been completed and returned a soltuion </param>
	/// <returns> total unloading cost  </returns>
private:
	int calcUnloadCost(std::vector<Edge<E>*> &jobList)
	{
		int cost = 0;
		for (auto e : jobList)
		{
			cost += e->getEnd()->getNodeCost();
		}
		return cost;
	}

};
