//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Output.h"
#include "Edge.h"

void Output::printPath(std::vector<Edge<std::wstring>*> &p)
{

	if (p.size() > 0)
	{

		for (auto e : p)
		{
//JAVA TO C++ CONVERTER TODO TASK: There is no native C++ equivalent to 'toString':
			std::wcout << L"\n" << e->toString();
		}
		std::wcout << std::endl;
	}
	else
	{
		std::wcout << L"No Solution";
	}
}
