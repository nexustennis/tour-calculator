#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include <string>
#include <vector>
#include <iostream>

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class Edge;

/// <summary>
/// Freight System - Output Class, used for delegation
/// </summary>

class Output
{

	/// <summary>
	/// prints the path
	/// </summary>
	/// <param name="p"> - list of edges travelled in A* search
	/// @pre p contains valid edges
	/// @post prints out either the path travelled or no solution if p = null </param>
public:
	virtual void printPath(std::vector<Edge<std::wstring>*> &p);
};
