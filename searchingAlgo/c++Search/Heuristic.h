#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class State;

/// <summary>
/// Generic Interface for heuristic
/// 
/// </summary>

template<typename E>
class Heuristic
{

	/// <summary>
	/// Estimates the distance to the goal state, based on the current state 
	/// </summary>
	/// <param name="state"> - current state
	/// @pre state != null </param>
	/// <returns> heuristic cost </returns>
public:
	virtual int getBestEstimate(State<E> *state) = 0;

};
