#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include <string>
#include <vector>

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class FreightSystemGraph;
template<typename E>
class Edge;

/// <summary>
/// Freight System - Builder class, builds the graph, used for delegation
/// </summary>

class Builder
{

private:
	FreightSystemGraph<std::wstring> *map;


	/// <summary>
	/// Constructor for builder class
	/// 
	/// @post concrete graph of nodes labelled with string is initialised
	/// </summary>

public:
	virtual ~Builder()
	{
		delete map;
	}

	Builder();

	/// <summary>
	/// Builds the graph
	/// </summary>
	/// <param name="input"> - array of strings, contains the scanned input
	/// @pre input != null, input is valid according to the spec 
	/// @post Either a node is added, a edge is added, a job is added </param>
	virtual void buildMap(std::vector<std::wstring> &input);

	/// <summary>
	/// Get the graph
	/// 
	/// @pre graph is initialised </summary>
	/// <returns> graph is returned  </returns>
	virtual FreightSystemGraph<std::wstring> *getMap();

	/// <summary>
	/// get the list of jobs
	/// 
	/// @pre graph (graph contains jobList) is initialised </summary>
	/// <returns> joblist is returned  </returns>
	virtual std::vector<Edge<std::wstring>*> getJobList();


};
