#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "State.h"

/// <summary>
/// Freight System - State Comparator class, use in the priority queue of the A*
/// </summary>

template<typename E>
class StateComparator : public Comparator<State<E>*>
{

	/// <summary>
	/// compared method
	/// </summary>
	/// <param name="s1"> - a state to be compared 
	///        s2 - the other state to be compared
	/// @pre s1 != null, s2 != null </param>
	/// <returns> -1 or 1 depending on the f costs of the states </returns>
public:
	int compare(State<E> *s1, State<E> *s2) override
	{
		int s1Estimate = s1->getFCost();
		int s2Estimate = s2->getFCost();

		int result = 0;
		if (s1Estimate <= s2Estimate)
		{
			result = -1;
		}
		else if (s1Estimate > s2Estimate)
		{
			result = 1;
		}
		else
		{
			result = 0;
		}
		return result;
	}

};
