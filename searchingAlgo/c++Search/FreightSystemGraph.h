#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Graph.h"
#include "Node.h"
#include "Edge.h"
#include <vector>
#include <list>

/// <summary>
/// Concrete generic graph represention
/// </summary>

template<typename E>
class FreightSystemGraph : public Graph<E>
{

private:
	std::vector<Node<E>*> nodeList;
	std::vector<Edge<E>*> jobList;


	/// <summary>
	/// Constructor for the graph
	/// 
	/// @post new instance of the graph will be created
	/// </summary>
public:
	FreightSystemGraph()
	{
		this->nodeList = std::list<Node<E>*>();
		this->jobList = std::list<Edge<E>*>();
	}

	/// <summary>
	/// adds a node to the graph
	/// </summary>
	/// <param name="node"> - label to identify the node
	///        nodeCost - the unloading cost of the node
	/// @pre node != null, nodeCost >= 0
	/// @post nodeList is update with new node </param>
	void addNode(E node, int nodeCost) override
	{
		// since "node" is a arbitary object of type E
		// we have to first create a new node instance then add
		// this adds a node - where "node" is the name and with a list of neighbours
		Node<E> tempVar(node,nodeCost);
		this->nodeList.push_back(&tempVar);
	}


	/// <summary>
	/// adds a edge to the graph
	/// </summary>
	/// <param name="node1"> - label for the start node of the edge
	///        node2 - label for the end node of the edge
	///        edgeCost - the weighting of the node, in this case the travel cost
	/// @pre node1 and node2 exists within the graph, edgeCost > 0
	/// @post the connections are added to the adjancey list of the nodes, undirected  </param>
	void addEdge(E node1, E node2, int edgeCost) override
	{
		Node<E> *start = findNode(node1);
		Node<E> *end = findNode(node2);

		start->addConnection(start,end,edgeCost);
		end->addConnection(end,start,edgeCost);
	}


	/// <summary>
	/// Searches the graph for the node, based on the given label
	/// </summary>
	/// <param name="name"> - label to identify node </param>
	/// <returns> either the node, if found, or null if not found </returns>
	Node<E> *findNode(E name) override
	{

		for (auto node : this->nodeList)
		{
			if (Objects::equals(node->getName(),name))
			{
				return node;
			}
		}
		return nullptr;
	}

	/// <summary>
	/// Searches the graph for a specified edge
	/// </summary>
	/// <param name="n1"> - label to identify start node
	///        n2 - label to identify end node </param>
	/// <returns> either the edge, if found, or null if not found </returns>
private:
	Edge<E> *findEdge(E n1, E n2)
	{
		Node<E> *start = findNode(n1);
		Node<E> *end = findNode(n2);

		for (auto e : start->getConnections())
		{
			if (Objects::equals(e->getEnd()->getName(),end->getName()))
			{
				return e;
			}
		}
		return nullptr;
	}

	/// <summary>
	/// Adds a graph edge to the list of jobs 
	/// </summary>
	/// <param name="x"> - label to identify start node
	///        y - label to identify end node
	/// @pre x and y are existing nodes and are connected by an edge
	/// @post adds a job to the job list </param>
public:
	virtual void addJob(E x, E y)
	{
		jobList.push_back(findEdge(x,y));
	}

	/// <summary>
	/// get the job list of edges that must be cover in the A* search 
	/// </summary>
	/// <returns> jobList </returns>
	virtual std::vector<Edge<E>*> getJobList()
	{
		return this->jobList;
	}
};
