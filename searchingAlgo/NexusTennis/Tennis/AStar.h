//
// Created by oscar on 30/12/17.
//

#ifndef TENNIS_ASTAR_H
#define TENNIS_ASTAR_H


#include "Graph.h"
#include "Edge.h"
#include "Heuristic.h"
#include "State.h"

#include <vector>
#include <list>
#include <algorithm>
#include <iostream>
#include <queue>



class Compare {

public:
    bool operator() (State * s1, State * s2) {

        int s1Estimate = s1->getFCost();
        int s2Estimate = s2->getFCost();


        return s1Estimate > s2Estimate;
    }
};

class AStar {

public:
    static constexpr int MAX_NODES = 100000;
    vector<Edge*> bestPath(Graph *g, string start, string end, Heuristic *h);
    vector<Edge*> constructPath(State *state, int n, int unloadCost);

private:


};


template<
        class T,
        class Container = std::vector<T>,
        class Compare = std::less<typename Container::value_type>
> class MyQueue : public std::priority_queue<T, Container, Compare>
{
public:
    typedef typename
    std::priority_queue<
            T,
            Container,
            Compare>::container_type::const_iterator const_iterator;

    bool found = false;

    const_iterator find(const T&val) {
        this->found = false;
        auto first = this->c.cbegin();
        auto last = this->c.cend();
        while (first!=last) {
            if (first==val) {
                this->found = true;
                return first;
            }
            ++first;
        }
        this->found = false;
        return last;
    }

    bool getFound() { return this->found;}


};




#endif //TENNIS_ASTAR_H
