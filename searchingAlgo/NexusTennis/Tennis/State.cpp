//
// Created by oscar on 30/12/17.
//

#include "State.h"
#pragma once

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

#include "Node.h"
#include "Edge.h"
#include <vector>
#include <list>

/// <summary>
/// Freight System - Generic State Class
/// </summary>


/// <summary>
/// Contructer for the state
/// each state is given its own copy of the job list
///
/// where the state contains all the info about the A* search at that node
/// </summary>
/// <param name="node"> - the node of that particular state
///        edge - the edge given to the state, used to determine if the state is a job state
///        parent - parent state
/// @pre node != null
/// @post new state is created, and jobList and jobToDo is either updated, or set to null and -1 respectively </param>
State::~State()
{
    delete node;
    delete edge;
    delete parent;
}

State::State(Node *node, Edge *edge, State *parent)
{
    this->node = node;
    this->edge = edge;
    this->parent = parent;
}


/* === Getters === */


/// <summary>
/// gets the node of this instance of the state
/// </summary>
/// <returns> node </returns>
Node* State::getNode()
{
    return this->node;
}

/// <summary>
/// gets the parent state of this instance of state
/// </summary>
/// <returns> parent state </returns>
State* State::getParent()
{
    return this->parent;
}

/// <summary>
/// gets the edge of this instance of state
/// </summary>
/// <returns> edge </returns>
Edge* State::getEdge()
{
    return this->edge;
}

/// <summary>
/// gets the name of the state node
/// </summary>
/// <returns> name of the state node  </returns>
string State::getName()
{
    return this->getNode()->getName();
}

/// <summary>
/// gets the adjancey list of the state node
/// </summary>
/// <returns> list of edges connected to state node </returns>
vector<Edge*> * State::getConnections()
{
    return this->getNode()->getConnections();
}

/// <summary>
/// gets the distance from the start to the current state
/// </summary>
/// <returns> dist from start </returns>
int State::getGCost()
{
    return this->getNode()->getGCost();
}

/// <summary>
/// gets the estimated distance to the goal state
/// </summary>
/// <returns> estimate to goal state </returns>
int State::getHCost()
{
    return this->getNode()->getHCost();
}

/// <summary>
/// f cost = dist from start + estimate to goal state
/// </summary>
/// <returns> f cost </returns>
int State::getFCost()
{
    return this->getNode()->getFCost();
}

/// <summary>
/// gets the edge cost of the state
/// </summary>
///  <returns> edge cost </returns>
int State::getEdgeCost()
{
    return this->getEdge()->getEdgeCost();
}

/* === Setters === */

/// <summary>
/// sets the cost from the start
/// </summary>
/// <param name="x"> - new g cost (new cost from the start)
/// @pre x >= 0
/// @post gCost of node will be updated </param>
void State::setGCost(int x)
{
    this->getNode()->setGCost(x);
}

/// <summary>
/// sets the estimated dist to the goal state
/// </summary>
/// <param name="x"> - new h cost (new estimate to goal state)
/// @pre x >= 0, and x <= actual dist to the goal state
/// @post gCost of node will be updated </param>
void State::setHCost(int x)
{
    this->getNode()->setHCost(x);
}

/* === Methods === */


/// <summary>
/// gets the children of the current state
/// </summary>
/// <returns> List of of the states that following the current state </returns>
list<State *> State::getChildren()
{

    list<State *> * children = new list<State*>();

    for (Edge * e : *this->getConnections())
    {

        State *child = new State(e->getNode2(),e,this);

        children->push_back(child);
    }

    return *children;
}
