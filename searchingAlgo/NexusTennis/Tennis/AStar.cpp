//
// Created by oscar on 30/12/17.
//

#include "AStar.h"

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

using namespace std;

/// <summary>
/// This method calculates the best path through a graph
/// given a subset of edges that must be passed through
///
/// Implementation of the A* Algorithm, the goal state is the where the
/// path travelled covers the subset of edges, joblist.
/// </summary>
/// <param name="g"> - graph to be searched
///        start - starting node label
///        jobList - subset of the graph edges, that must be covered in return path
///        h - heuristic used in A* search
/// @pre g != null, start != null, jobList != null, h != null </param>
/// <returns> List of generic edges </returns>




vector<Edge*> AStar::bestPath(Graph *g, string start, string end, Heuristic *h){

    /* Setting up open priority queue and closed list */

    //initialize the open list
    //initialize the closed list
    MyQueue<State*, vector<State*>, Compare > open = MyQueue<State*, vector<State*>, Compare >();
    vector<State*> closed = vector<State*>();

    /* keeps track of nodes popped of the queue */
    int nExpanded = 0;

    /* Add initial state to the pq and setting the g and h costs */
    State* curr = new State(g->findNode(start),nullptr,nullptr);
    curr->setGCost(0);
    curr->setHCost(0);

    //open.push(curr);
    cout << curr->getName() << " ------ \n";

    //put the starting node on the open list (you can leave its f at zero)
    open.push(curr);

    // while the open list is not empty
    while (!open.empty())
    {


        /* pop highest priority state off queue (based on lowest f = g + h )*/
        // find the node with the least f on the open list, call it "q"
        // pop q off the open list

        curr = open.top();
        cout << curr->getName() << " curr __________\n";
        open.pop();

        // generate q's 8 successors
        vector<Edge*> *conns = curr->getConnections();

        for (Edge* e : *conns)
        {
            /* take in edge and parent node from which all  the data i need can be accessed */
            // and set their parents to q
            State *neighbor = new State((e->getNode2()),e,curr);
            cout << neighbor->getName() << " ---------- \n";

            // if successor is the goal, stop the search
            if(neighbor->getName() == end) return constructPath(neighbor,nExpanded,0);

            // successor.g = q.g + distance between successor and q
            // successor.h = distance from goal to successor
            // successor.f = successor.g + successor.h
            /* Dist from start */
            int gCost = curr->getGCost() + e->getEdgeCost();
            int hCost = h->getBestEstimate(neighbor);
            int fCost = gCost + hCost;

            cout << "P_queue>>>>>>>>>>>>>\n";
            bool inOpen = false;
            MyQueue<State*, vector<State*>, Compare > tmp_q = open; //copy the original queue to the temporary queue
            while (!tmp_q.empty())
            {
                State * q_element = tmp_q.top();
                cout << q_element->getName() << '\n';
                if(q_element->getName() == neighbor->getName()) {
                    inOpen = true;
                }
                tmp_q.pop();
            }

            cout << "END P_queue>>>>>>>>>>\n";

            /*
            open.find(neighbor);
            bool inOpen = false;

            if(open.getFound()) {
                inOpen = true;
            }*/
            cout << inOpen << " open ---------- \n";
            bool inClosed = false; //find(closed.begin(), closed.end(), neighbor) != closed.end();
            for(State* s : closed) {
                if(s->getName() == neighbor->getName()) {
                    inClosed = true;
                }
            }
            cout << inClosed << " closed ---------- \n";

            /*
            if a node with the same position as successor is in the OPEN list \
                which has a lower f than successor, skip this successor
            if a node with the same position as successor is in the CLOSED list \
                which has a lower f than successor, skip this successor
            otherwise, add the node to the open list
             */
            /* if new state f cost is cheaper than the old f cost
               or if the f cost is has not been set (not yet explored) */
            if (inClosed)
            {
                if(fCost < curr->getFCost())
                {
                    neighbor->setGCost(gCost);
                    neighbor->setHCost(hCost);
                }
            }
            else if (inOpen)
            {
                if(fCost < curr->getFCost()) {
                    neighbor->setGCost(gCost);
                    neighbor->setHCost(hCost);
                }

            }
            else
            {
                neighbor->setGCost(gCost);
                neighbor->setHCost(hCost);
                open.push(neighbor);
            }
        }

        /* After so many nodes give up */
        if (nExpanded > MAX_NODES)
        {
            return constructPath(curr,nExpanded,0);
        }


        // push q on the closed list
        cout << curr->getName() << " closing ________\n";
        closed.push_back(curr);

        nExpanded++;
    }

    return constructPath(curr,nExpanded,0);
}


/// <summary>
/// Constructs the path travelled through
/// </summary>
/// <param name="state"> - the final state
///        n - nodes expanded
///        unloadCost - the unloading cost of the jobs that must be accounted for
/// @pre state != null
/// @post expanded nodes and total cost are printed
///       note: to be fixed (my orginal intention was to do all the printing in output but I ran out of time!) </param>
/// <returns> List of the travel edges </returns>
vector<Edge*> AStar::constructPath(State *state, int n, int unloadCost)
{

    wcout << n << L" expanded" << endl;

    wcout << L"cost = " << state->getGCost() << unloadCost;

    vector<Edge*> path;

    State *curr = state;

    while (curr->getParent() != nullptr)
    {

        path.insert(path.begin(),curr->getEdge());
        curr = curr->getParent();
    }
    return path;

}