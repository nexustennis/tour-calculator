//
// Created by oscar on 19/12/17.
//

#include <iostream>
#include <utility>
#include "Graph.h"

using namespace std;

/// <summary>
/// Concrete generic graph represention
/// </summary>

Graph::Graph(vector<Node *> nodeList)  {
    this->nodeList = std::move(nodeList);
}

vector<Node *> * Graph::getNodeList() {
    return &this->nodeList;
}

/// <summary>
/// adds a node to the graph
/// </summary>
/// <param name="node"> - label to identify the node
///        nodeCost - the unloading cost of the node
/// @pre node != null, nodeCost >= 0
/// @post nodeList is update with new node </param>

void Graph::addNode(string node, string dateStart, string dataEnd) {
    Node* temp = new Node(std::move(node), std::move(dateStart), move(dataEnd));
    this->nodeList.push_back(temp);
}

/// <summary>
/// adds a edge to the graph
/// </summary>
/// <param name="node1"> - label for the start node of the edge
///        node2 - label for the end node of the edge
///        edgeCost - the weighting of the node, in this case the travel cost
/// @pre node1 and node2 exists within the graph, edgeCost > 0
/// @post the connections are added to the adjancey list of the nodes, undirected  </param>

void Graph::addEdge(string nodeString1, string nodeString2, int edgeCost) {
    Node *node1 = findNode(nodeString1);
    Node *node2 = findNode(nodeString2);

    node1->addConnection(node1, node2, edgeCost);
    node2->addConnection(node2, node1, edgeCost);
}


/// <summary>
/// Searches the graph for the node, based on the given label
/// </summary>
/// <param name="name"> - label to identify node </param>
/// <returns> either the node, if found, or null if not found </returns>

Node * Graph::findNode(string name) {
    for (Node *node : this->nodeList) {

        if (node->getName() == name) {
            return node;
        }
    }

    return nullptr;
}

/// <summary>
/// Searches the graph for a specified edge
/// </summary>
/// <param name="n1"> - label to identify start node
///        n2 - label to identify end node </param>
/// <returns> either the edge, if found, or null if not found </returns>

Edge * Graph::findEdge(string nodeString1, string nodeString2) {
    Node *node1 = findNode(nodeString1);
    Node *node2 = findNode(nodeString2);

    for (unsigned int i = 0; i < node1->getConnections()->size(); ++i) {
        if (node1->getConnections()->at(i)->getNode1()->getName() == node2->getName() || node1->getConnections()->at(i)->getNode2()->getName() == node2->getName()) {
            return node1->getConnections()->at(i);
        }   
    }
    
    return nullptr;
}