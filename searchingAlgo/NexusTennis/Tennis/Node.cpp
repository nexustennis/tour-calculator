//
// Created by oscar on 19/12/17.
//

#include "Node.h"

#include <utility>

using namespace std;

/// <summary>
/// Node class - node is a generic type E
/// -> weight is unloading cost
/// </summary>

Node::Node(string name, string dateStart, string dateEnd) {
    this->name = std::move(name);
    this->dateStart = std::move(dateStart);
    this->dateEnd = std::move(dateEnd);
    this->connections = std::vector<Edge*>();

    /* Cost are initialised to 0
       then are set when explored */

    this->gCost = 0;
    this->hCost = 0;
    this->fCost = 0;
}

/* === Getters === */

/// <summary>
/// gets the name of the node
/// </summary>
/// <returns> name of the node  </returns>

string Node::getName() {
    return this->name;
}

/// <summary>
/// gets the node cost - unloading cost
/// </summary>
/// <returns> node cost </returns>

string Node::getDate() {
    return this->dateStart;
}

/// <summary>
/// gets the adjancey list of the node
/// </summary>
/// <returns> list of edges connected to node </returns>

vector<Edge *> * Node::getConnections() {
    return &this->connections;
}


/// <summary>
/// gets the distance from the start to the current node
/// </summary>
/// <returns> dist from start </returns>

int Node::getGCost() {
    return this->gCost;
}

/// <summary>
/// gets the estimated distance to the goal
/// </summary>
/// <returns> estimate to the goal </returns>

int Node::getHCost() {
    return this->hCost;
}

/// <summary>
/// f cost = dist from start + estimate to the goal
/// </summary>
/// <returns> f cost </returns>

int Node::getFCost() {
    return this->getGCost() + this->getHCost();
}

/* === Setters === */

/// <summary>
/// sets the cost from the start
/// </summary>
/// <param name="x"> - new g cost (new cost from the start)
/// @pre x >= 0
/// @post gCost of node will be updated </param>

void Node::setGCost(int x) {
    this->gCost = x;
}

/// <summary>
/// sets the estimated dist to the goal
/// </summary>
/// <param name="x"> - new h cost (new estimate to the goal)
/// @pre x >= 0, and x <= actual dist to the goal
/// @post gCost of node will be updated </param>

void Node::setHCost(int x) {
    this->hCost = x;
}

/* === Methods === */

/// <summary>
/// adds a connection into the nodes in the adjancey list
/// </summary>
/// <param name="start"> - label for the start node of the edge
///        end - label for the end node of the edge
///        edgeWeight - the weighting of the edge
/// @pre node1 and node2 exists within the graph, edgeCost > 0
/// @post the connections are added to the adjancey list of the node  </param>

void Node::addConnection(Node *node1, Node *node2, int edgeWeight) {
    Edge *edge = new Edge(node1, node2, edgeWeight);
    this->getConnections()->push_back(edge);
}