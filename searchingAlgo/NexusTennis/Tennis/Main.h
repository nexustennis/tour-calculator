//
// Created by oscar on 24/12/17.
//

#ifndef TENNIS_MAIN_H
#define TENNIS_MAIN_H

#include<iostream>
#include<list>
#include <fstream>
#include<cmath>
#include <cstdlib>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <iterator>
#include "Graph.h"

class Graph;

class Main {

    public:
        Graph g = Graph(vector<Node*>());
        void buildGraph(vector<string> data);
        vector<string> split(const string &s, char delim);

};

#endif //TENNIS_MAIN_H
