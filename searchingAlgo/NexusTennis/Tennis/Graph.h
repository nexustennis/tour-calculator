//
// Created by oscar on 19/12/17.
//

#ifndef TENNIS_GRAPH_H
#define TENNIS_GRAPH_H

#include <string>
#include "Node.h"
#include "Edge.h"

using namespace std;

class Graph {

    private:
        vector<Node *> nodeList = vector<Node *>();

    public:
        Graph(vector<Node *> nodeList);
        void addNode(string node, string dateStart, string dateEnd);
        void addEdge(string node1, string node2, int weight);
        Node * findNode(string name);
        vector<Node *> * getNodeList();
        Edge * findEdge(string n1, string n2);
        
};

#endif //TENNIS_GRAPH_H