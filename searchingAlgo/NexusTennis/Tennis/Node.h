//
// Created by oscar on 19/12/17.
//

#ifndef TENNIS_NODE_H
#define TENNIS_NODE_H

#include <iostream>
#include <vector>
#include <list>
#include <string>
#include "Edge.h"

using namespace std;

class Edge;

class Node {

    private:
        string name = string();
        vector<Edge*> connections = vector<Edge*>();
        string dateStart = string();
        string dateEnd = string();

        int fCost = 0;
        int gCost = 0;
        int hCost = 0;

    public:
        Node(string name, string dateStart, string dateEnd);
        string getName();
        void addConnection(Node *node1, Node *node2, int edgeWeight);
        std::vector<Edge *> * getConnections();

        string getDate();
        int getGCost();
        int getHCost();
        int getFCost();
        void setGCost(int x);
        void setHCost(int x);
        
};

#endif //TENNIS_NODE_H