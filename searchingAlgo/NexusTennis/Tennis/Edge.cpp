//
// Created by oscar on 19/12/17.
//

#include "Edge.h"

using namespace std;

Edge::Edge(Node *node1, Node *node2, int edgeCost) {
    this->edgeCost = edgeCost;
    this->node1 = node1;
    this->node2 = node2;
    this->type = L"Empty";
}

/* === Getters === */

/// <summary>
/// Gets the edges cost
///
/// @pre this instance of the class has been initialised </summary>
/// <returns> edge cost of this instance </returns>

int Edge::getEdgeCost() {
    return this->edgeCost;
}

/// <summary>
/// Gets the node at the start of the edge
///
/// @pre this instance of the class has been initialised </summary>
/// <returns> start node </returns>

Node * Edge::getNode1() {
    return this->node1;
}

/// <summary>
/// Gets the node at the end of the edge
///
/// @pre this instance of the class has been initialised </summary>
/// <returns> end node </returns>

Node * Edge::getNode2() {
    return this->node2;
}

/// <summary>
/// Gets the type of edges, either "Empty" or "Job"
///
/// @pre this instance of the class has been initialised </summary>
/// <returns> type string </returns>

wstring Edge::getEdgeType() {
    return this->type;
}

/* === Setters === */

/// <summary>
/// Set the type of edge, either "Empty" or "Job"
/// </summary>
/// <param name="x"> - type
/// @pre this instance of the class has been initialised, x != null
/// @post type param will be updated </param>
/// <returns> type string </returns>

void Edge::setEdgeType(const wstring &x) {
    this->type = x;
}

/* === Methods === */

/// <summary>
/// Equals method
/// </summary>
/// <param name="obj"> - object to be compared </param>
/// <returns> true or false depending on whether the starting and ending nodes are equal </returns>

bool Edge::equals(void *obj) {
    // self check
    if (this == obj) {
        return true;
    }
    
    // null check
    if (obj == nullptr) {
        return false;
    }
    
    Edge *o = static_cast<Edge*>(obj);

    // field comparison
    if (this->getNode1() == o->getNode1() && this->getNode2() == o->getNode2()) {
        return true;
    }
    
    return false;
}