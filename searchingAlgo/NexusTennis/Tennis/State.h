//
// Created by oscar on 30/12/17.
//

#ifndef TENNIS_STATE_H
#define TENNIS_STATE_H

#include "Node.h"

using namespace std;

class State {

private:
    Node *node = nullptr;
    Edge *edge = nullptr;
    State *parent = nullptr;
public:
    ~State();
    State(Node *node, Edge *edge, State *parent);
    Node *getNode();
    State *getParent();
    Edge *getEdge();
    string getName();
    vector<Edge*> * getConnections();
    int getGCost();
    int getHCost();
    int getFCost();
    int getEdgeCost();
    void setGCost(int x);
    void setHCost(int x);
    list<State *> getChildren();

};


#endif //TENNIS_STATE_H
