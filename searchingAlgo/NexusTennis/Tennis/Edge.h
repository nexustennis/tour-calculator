//
// Created by oscar on 19/12/17.
//

#ifndef TENNIS_EDGE_H
#define TENNIS_EDGE_H

#pragma once

#include <string>
#include "Node.h"

using namespace std;

class Node;

class Edge {

    private:
        int edgeCost = 0;
        Node *node1;
        Node *node2;
        wstring type;

    public:
        Edge(Node *node1, Node *node2, int edgeCost);
        int getEdgeCost();
        Node * getNode1();
        Node * getNode2();
        wstring getEdgeType();
        void setEdgeType(const wstring &x);
        bool equals(void *obj);
    
};

#endif //TENNIS_EDGE_H