#include "Main.h"
#include "State.h"
#include "Heuristic.h"
#include "AStar.h"

using namespace std;

void Main::buildGraph(vector<string> data) {
    if (data[0] == "AddNode") {
        this->g.addNode(data[1], data[2], data[3]);
    }
    
    else if (data[0] == "AddEdge") {
        this->g.addEdge(data[1], data[2], stoi(data[3]));
    }
}

vector<string> Main::split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    
    return tokens;
}

int main (int argc, char *argv[]) {
    if (argc != 4) {
        cout << "usage: " << argv[0] << " <filename>\n";
        return 0;
    }

    cout << "Starting...\n";

    Main m = Main();
    vector<Node *> nl = vector<Node *>();
    m.g = Graph(nl);

    ifstream file(argv[1]);

    if (!file.is_open()) {
        cout << "could not open file!\n";
    } else {
        string line;
        cout << "building graph\n";
        while (getline(file, line)) {
            vector<string> results = m.split(line, ' ');
            m.buildGraph(results);
        }
    }

    cout << "done\n";
    AStar as = AStar();

    vector<Edge *> path = as.bestPath(&(m.g), argv[2], argv[3], new Heuristic());

    cout << "\n";
    for(Edge* e : path ) {
        cout << e->getNode1()->getName() << '-';
        cout << e->getNode2()->getName() << " \n";
    }

    /*
    cout << "Node List Size: " << m.g.getNodeList()->size() << "\n";
    cout << "Node List:\n";
    
    for (unsigned int i = 0; i < m.g.getNodeList()->size(); ++i ) {
        cout << m.g.getNodeList()->at(i)->getName() << "\n";
        
        for (unsigned int j = 0; j < m.g.getNodeList()->at(i)->getConnections()->size(); ++j) {
            cout << "  ";
            cout << m.g.getNodeList()->at(i)->getConnections()->at(j)->getNode1()->getName() << " ";
            cout << m.g.getNodeList()->at(i)->getConnections()->at(j)->getNode2()->getName() << "\n";
        }
    }*/



}