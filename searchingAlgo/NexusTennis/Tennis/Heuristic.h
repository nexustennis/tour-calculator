//
// Created by oscar on 31/12/17.
//

#ifndef TENNIS_HEURISTIC_H
#define TENNIS_HEURISTIC_H


#include "State.h"

class Heuristic {

public:

    int getBestEstimate(State *state);

};


#endif //TENNIS_HEURISTIC_H
