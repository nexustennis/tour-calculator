/*
 * Graph.h
 *
 *  Created on: 18 Dec. 2017
 *      Author: oscar
 */

#ifndef GRAPH_H_
#define GRAPH_H_

//========================================================================
// This conversion was produced by the Free Edition of
// Java to C++ Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================
#include "Node.h"
#include "Edge.h"
#include <vector>
#include <list>

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
template<typename E>
class Node;

/// <summary>
/// Generic Graph Interface
///
/// nodes - defined as arbitary objects
/// edges - a binary relation between two objects with weights
/// </summary>

template<typename E>
class Graph
{

	/// <summary>
	/// adds a node to the graph
	/// </summary>
	/// <param name="node"> - label to identify the node
	///        nodeCost - the unloading cost of the node
	/// @pre node != null, nodeCost >= 0
	/// @post nodeList is update with new node </param>
public:
	virtual void addNode(E node, int nodeCost) = 0;

	/// <summary>
	/// adds a edge to the graph
	/// </summary>
	/// <param name="node1"> - label for the start node of the edge
	///        node2 - label for the end node of the edge
	///        edgeCost - the weighting of the edge
	/// @pre node1 and node2 exists within the graph, edgeCost > 0
	/// @post the connections are added to the adjancey list of the node, undirected  </param>
	virtual void addEdge(E node1, E node2, int weight) = 0;

	/// <summary>
	/// Searches the graph for a specific node
	/// </summary>
	/// <param name="name"> - label to identify node </param>
	/// <returns> either the node, if found, or null if not found </returns>
	virtual Node<E> *findNode(E name) = 0;

};
